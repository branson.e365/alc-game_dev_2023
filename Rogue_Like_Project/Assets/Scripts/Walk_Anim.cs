using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk_Anim : MonoBehaviour
{
    private Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("isWalking", false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
            anim.SetBool("isWalking", true);
        else if (Input.GetKeyDown(KeyCode.I))
            anim.SetBool("isWalking", false);
    }
}
