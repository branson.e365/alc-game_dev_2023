using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour
{
    [Header("Map Properties")]
    public int mapWidth = 7;
    public int mapHeight = 7;
    public float roomSize = 12f;
    public float tileSize = 0.16f;
    public int roomsToGenerate = 12;


    private int roomCount;
    private bool roomsInstantiated;

    //store the origin of the first room to generate procedural dungeon.
    private Vector2 firstRoomPos;

    // a 2d boolean array to map out the level
    private bool[,] map;
    // the room prefab to instantiate
    public GameObject roomPrefab;

    private List<Room> roomObjects = new List<Room>();

    //creating a singleton
    public static Generation instance;

    public Vector2 generalDirection;

    [Header("Spawn Chances")]
    public float enemySpawnChance;
    public float coinSpawnChance;
    public float healthSpawnChance;

    public int maxEnemiesPerRoom;
    public int maxCoinsPerRoom;
    public int maxHealthPerRoom;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        // random seed assigned to a random number generator
        Random.InitState(1234);
        Generate();
    }

    public void Generate()
    {
        

        //creata a new map of the specified size
        map = new bool[mapWidth, mapHeight];

        //check to see if we can place a room in the center of the map
        CheckRoom(3, 3, 0, Vector2.zero, true);
        InstantiateRooms();

        //find the player in the scene, and the postion them inside the first room
        FindObjectOfType<Player_Controller_2D>().transform.position = firstRoomPos * roomSize * tileSize;
    }

    void CheckRoom(int x, int y, int remaining, Vector2 generalDirection, bool firstRoom = false)
    {
        // if we have generated all of the rooms that we need, stop checking the rooms.

        if (roomCount >= roomsToGenerate)
        {
            return;
        }

        // if this is outside  the bounds of the actual map, stop the function.
        if(x < 0 || x > mapWidth - 1 || y < 0 || y > mapHeight - 1)
        {
            return;
        }

        //if this is not the first room, and there is no more room to check, stop the function.

        if (firstRoom == false && remaining <= 0)
        {
            return;
        }

        //if the given map title is already occupied, stop the function
        if (map[x,y] == true)
            return;

        // if this is the first room, store the room position.
        if (firstRoom == true)
            firstRoomPos = new Vector2(x,y);
        //add oneto roomCount and set the map tile to be true
        roomCount++;
        map[x,y] = true;

        // north will be true if the random value is greater than 0.2f (if generalDirection == up) or greater than 0.8f (when generalDirection is up).
        bool north = Random.value > (generalDirection == Vector2.up ? 0.2f : 0.8f);
        bool south = Random.value > (generalDirection == Vector2.down ? 0.2f : 0.8f);
        bool east = Random.value > (generalDirection == Vector2.right ? 0.2f : 0.8f);
        bool west = Random.value > (generalDirection == Vector2.left ? 0.2f : 0.8f);

        int maxRemaining = roomsToGenerate / 4;

        // if north is true, make a room one tile from the current.

        if (north || firstRoom)
            CheckRoom(x,y + 1, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.up : generalDirection);
        if (south || firstRoom)
            CheckRoom(x,y - 1, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.down : generalDirection);
        if (east || firstRoom)
            CheckRoom(x + 1,y, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.right : generalDirection);
        if (west || firstRoom)
            CheckRoom(x - 1,y, firstRoom ? maxRemaining : remaining - 1, firstRoom ? Vector2.left : generalDirection);

    }


    void InstantiateRooms()
    {
        if (roomsInstantiated)
            return;

        roomsInstantiated = true;

        for (int x = 0; x < mapWidth; ++x)
        {
            for (int y = 0; y < mapHeight; ++y)
            {
                if (map[x,y] == false)
                    continue;

                //instantiate a new room prefab
                GameObject roomObj = Instantiate(roomPrefab, new Vector3(x, y, 0) * roomSize * tileSize, Quaternion.identity);
                //get refrence to the room script of the new room object
                Room room = roomObj.GetComponent<Room>();

                //if we're within the boundary of the map, and if there is room above us
                if(y < mapHeight - 1 && map[x, y + 1] == true)
                {
                    //enable the north door and disable the north wall
                    room.northDoor.gameObject.SetActive(true);
                    room.northWall.gameObject.SetActive(false);
                }
                //if we're within the boundary of the map, and if there is room below us
                if (y > 0 && map[x, y -1] == true)
                {
                    //enable the south door and disable the south wall
                    room.southDoor.gameObject.SetActive(true);
                    room.southWall.gameObject.SetActive(false);
                }
                //if we're within the boundary of the map, and if there is room right of us
                if (x < mapWidth - 1 && map[x + 1, y] == true)
                {
                    //enable the east door and disable the east wall
                    room.eastDoor.gameObject.SetActive(true);
                    room.eastWall.gameObject.SetActive(false);
                }
                //if we're within the boundary of the map, and if there is room left of us
                if (x > 0 && map[x -1, y] == true)
                {
                    //enable the west door and disable the west wall
                    room.westDoor.gameObject.SetActive(true);
                    room.westWall.gameObject.SetActive(false);
                }

                //if this is not the first room, call generate interior
                if (firstRoomPos != new Vector2(x,y))
                    room.GenerateInterior();

                //add the room to the room objects list
                roomObjects.Add(room);
            }
        }

        //after loooping through every element inside the map array, call CalculateKeyAndExit().
        CalculateKeyandExit();
    }

    //places the key and exit in the level
    void CalculateKeyandExit()
    {
        float maxDist = 0;
        Room a = null;
        Room b = null;

        foreach(Room aRoom in roomObjects)
        {
            foreach(Room bRoom in roomObjects)
            {
                // compare each of the rooms to find out which pair is the furthest apart
                float dist = Vector3.Distance(aRoom.transform.position, bRoom.transform.position);

                if(dist > maxDist)
                {
                    a = aRoom;
                    b = bRoom;
                    maxDist = dist;
                }
            }
        }
        //once room a and room b are found spawn in the key and exitdoor
        a.SpawnPrefab(a.keyPrefab);
        b.SpawnPrefab(b.exitdoorPrefab);
    }


}
