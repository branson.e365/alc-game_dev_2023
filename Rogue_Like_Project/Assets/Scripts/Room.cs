using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    //reference for the door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform eastDoor;
    public Transform westDoor;
    //refrence for the wall objects
    [Header("Wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform eastWall;
    public Transform westWall;
    //how many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideHeight;
    //Object to instantiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefab;
    public GameObject coinPrefab;
    public GameObject healthPrefab;
    public GameObject keyPrefab;
    public GameObject exitdoorPrefab;
    // list of positions to avoid instantiating new objects on top of old objects 
    private List<Vector3> usedPosition = new List<Vector3>();

    public void GenerateInterior()
    {
        //Generate coins, enemies, health packs, and tacos, etc.
        if(Random.value < Generation.instance.enemySpawnChance)
        {
            SpawnPrefab(enemyPrefab, 1, Generation.instance.maxEnemiesPerRoom + 1);
        }
        if (Random.value < Generation.instance.coinSpawnChance)
        {
            SpawnPrefab(coinPrefab, 1, Generation.instance.maxCoinsPerRoom + 1);
        }
        if (Random.value < Generation.instance.healthSpawnChance)
        {
            SpawnPrefab(healthPrefab, 1, Generation.instance.maxHealthPerRoom + 1);
        }
    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0)
    {
        int num = 1;
        if (min != 0 || max != 0)
            num = Random.Range(min, max);

        //for each of the prefabs
        for(int i = 0; i < num; i++)
        {
            //instantiate the prefab
            GameObject obj = Instantiate(prefab);
            //get the nearest tile to a random position inside the room
            Vector3 pos = transform.position + new Vector3(Random.Range(-insideHeight / 2, insideWidth / 2 + 1), Random.Range(-insideHeight /2, insideHeight /2 +1), 0);
            //place the prefab to the random position
            obj.transform.position = pos;
            // add the current position to the used position list
            usedPosition.Add(pos);
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
