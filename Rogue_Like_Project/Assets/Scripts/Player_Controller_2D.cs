using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Player_Controller_2D : MonoBehaviour
{
    public int curHP;
    public int maxHP;
    public int coins;

    public bool hasKey;

    public SpriteRenderer sr;

    //layer to avoid (mask)
    public LayerMask moveLayerMask;

    public float delay;

    public int damageAmount = 1;
    public float moveTileSize = 0.16f;

    void Move(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);
        //if there is no moveLayerMask detected in front of the player
        if(hit.collider == null)
        {
            transform.position += new Vector3(dir.x * moveTileSize, dir.y * moveTileSize, 0);
            //move all the enemies
            Enemy_Manager.instance.OnPlayerMove();

        }
    }


    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            Move(Vector2.up);
    }
    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            Move(Vector2.down);
    }
    public void OnMoveLeft(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            Move(Vector2.left);
    }
    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            Move(Vector2.right);
    }
    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.up);
    }
    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.down);
    }
    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.left);
    }
    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.right);
    }
    public void TakeDamage(int damageToTake)
    {
        curHP -= damageToTake;
        StartCoroutine(DamageFlash());
    }

    IEnumerator DamageFlash()
    {
        //get a reference to the default sprite color
        Color defaultColor = sr.color;
        //set the color to white
        sr.color = Color.red;
        //wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        // set the color back to its original color
        sr.color = defaultColor;
    }

    void TryAttack (Vector2 dir)
    {
        //ignore the layer 1 to 
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, 1 << 7);

        if(hit.collider != null)
        {
            hit.transform.GetComponent<Enemy>().TakeDamage(damageAmount);
        }
    }
}
